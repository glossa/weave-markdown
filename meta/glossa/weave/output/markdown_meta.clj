(ns glossa.weave.output.markdown-meta
  (:require
    [clojure.string :as str]))

(defn readme
  ([] (readme {:version (System/getenv "WEAVE_MARKDOWN_VERSION") :sha (System/getenv "WEAVE_MARKDOWN_SHA") :tag (System/getenv "WEAVE_MARKDOWN_TAG")}))
  ([{:keys [version sha tag]}]
   (when (or (str/blank? version)
             (str/blank? sha)
             (str/blank? tag))
     (throw (ex-info "Environment vars missing."
                     {:error :missing-env-vars
                      :WEAVE_MARKDOWN_VERSION version
                      :WEAVE_MARKDOWN_SHA sha
                      :WEAVE_MARKDOWN_TAG tag})))
   [:glossa.weave/doc
    [:h1 "Weave: Markdown Output"]
    [:p "This library implements rudimentary Markdown output for " [:a {:href "https://gitlab.com/glossa/weave"} "Weave"] " documents. This output format is designed for reading directly (e.g., at the REPL), so it does _not_ support arbitrary tags or HTML output."]
    (when-not (= version "skip")
      [:pre {:lang "clj"}
       (str
         ";; Git dep\n"
         (format "{dev.glossa/weave-markdown\n {:git/url \"https://gitlab.com/glossa/weave-markdown.git\"\n  :git/tag \"%s\"\n  :git/sha \"%s\"}}"
                 tag sha)
         "\n\n;; Maven dep at https://clojars.org/dev.glossa/weave-markdown\n"
         (format "{dev.glossa/weave-markdown {:mvn/version \"%s\"}}"
                 version))])
    [:p "Because this is Weave's default output format, no further configuration is required:"]
    [:pre {:lang "clj"}
     "(require '[glossa.weave :as weave])\n(weave/weave [:h1 \"Markdown Output!\"])"]

    [:h2 "Development"]
    [:p [:em "NB: Review the scripts and `deps.edn` files for necessary aliases (not all of which are defined in this repo)."]]
    [:p "Please run the setup script to populate Git hooks:"]
    [:pre {:lang "shell"} "./script/setup"]
    [:p "To start nREPL (bring your own `:cider` alias):"]
    [:pre {:lang "shell"} "./script/dev"]
    [:p "To run tests:"]
    [:pre {:lang "shell"} "./script/test"]
    [:p "To run everything that CI does:"]
    [:pre {:lang "shell"} "./script/ci"]

    [:h2 "License"]
    [:p "Copyright © Daniel Gregoire, 2021"]
    [:p "THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF THIS ECLIPSE PUBLIC LICENSE (\"AGREEMENT\"). ANY USE, REPRODUCTION OR DISTRIBUTION OF THE PROGRAM CONSTITUTES RECIPIENT'S ACCEPTANCE OF THIS AGREEMENT."]]))
