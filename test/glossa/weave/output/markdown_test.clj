(ns glossa.weave.output.markdown-test
  (:require
   [clojure.string :as str]
   [clojure.test :refer [deftest is]]
   [glossa.weave :as weave]
   [glossa.weave.output.markdown :as nut]))

(def test-env
  (weave/env {:output nut/output-format}))

(defn- stitched [strings]
  (str/join "" (interleave strings (repeat "\n"))))

(def weave-doc
  [::weave/doc
   {:author "Glossa Weave Maintainers"}
   [:h1 "Weave Markdown Document"]
   [:p [:strong "Testing."]]
   [:p [:em "Testing?"]]
   [:p [:a {:href "https://example.com"} "Example link."]]
   [:h2 "Unit Testing"]
   [:p "Use " [:code "is"] " to test " [:s "expectations"] " assertions."]
   [:pre {:lang :clj}
    (pr-str '(is (= 2 (inc 1))))]
   [:hr]
   [:p "And now for something completely different."]
   [:p [:code-whitespace "a b  c\td"]]
   [:pre-whitespace
    "e  f\tg\nh i\r\nWindows new line"]
   [:dl
    [:dt "Alpha:"]
    [:dd "First letter of Greek alphabet."]
    [:dt "Beta:"]
    [:dd "Second letter of Greek alphabet."]]])

(deftest test-weave
  (is (= (stitched
          ["# Weave Markdown Document #"
           ""
           "**Testing.**"
           ""
           "_Testing?_"
           ""
           "[Example link.](https://example.com)"
           ""
           "## Unit Testing ##"
           ""
           "Use `is` to test ~expectations~ assertions."
           ""
           "```clj"
           "(is (= 2 (inc 1)))"
           "```"
           ""
           "--------------------------------------------------------------------------------"
           ""
           "And now for something completely different."
           ""
           "`a␣b␣␣c»   d`"
           ""
           "```"
           "e␣␣f»   g↓"
           "h␣i↵↓"
           "Windows␣new␣line"
           "```"
           ""
           "   - **Alpha:** First letter of Greek alphabet."
           "   - **Beta:** Second letter of Greek alphabet."])
         (weave/weave test-env weave-doc))))

(def weave-doc|macros
  [::weave/doc
   [:ol
    [:li "alpha"]
    [:item "beta"]
    [:li "gamma"]]
   [:ul
    [:item "oh"]
    [:item "wow"]]
   [:blockquote
    "Seems, madam?"
    "Nay, it is, I know not 'seems'."]])

(deftest test-weave-macro-nodes
  (is (= (stitched
          ["   1. alpha"
           "   2. beta"
           "   3. gamma"
           ""
           "   - oh"
           "   - wow"
           ""
           "> Seems, madam?"
           "> Nay, it is, I know not 'seems'."])
         (weave/weave test-env weave-doc|macros))))

(def weave-doc|nested-lists
  [::weave/doc
   [:ul
    [:li "You could try:"
     [:ol
      [:li "First"]
      [:li "Second"]
      [:li "Third, and if so:"
       [:ul
        [:li "Remember this."]
        [:li "Remember that."]
        [:li "And consider these translations:"
         [:dl
          [:dt "яблоко"] [:dd "apple"]
          [:dt "хлеб"] [:dd "bread"]]]]]]]
    [:li "Also, consider..."]]])

(deftest test-weave|nested-lists
  (is (= (stitched
          ["   - You could try:"
           "      1. First"
           "      2. Second"
           "      3. Third, and if so:"
           "         - Remember this."
           "         - Remember that."
           "         - And consider these translations:"
           "            - **яблоко** apple"
           "            - **хлеб** bread"
           "   - Also, consider..."])
         (weave/weave test-env weave-doc|nested-lists))))

(def weave-doc|nested-list
  [::weave/doc
   [:ol
    [:li "You could stop."]
    [:li "Or you could try:"
     [:ul
      [:li "This"]]]
    [:li "Or"
     [:ul
      [:li "This and"]
      [:li "That"]]]]])

(deftest test-weave|nested-list
  (is (= (stitched
          ["   1. You could stop."
           "   2. Or you could try:"
           "      - This"
           "   3. Or"
           "      - This and"
           "      - That"])
         (weave/weave test-env weave-doc|nested-list))))

(def weave-doc|nested-formatting
  [::weave/doc
   [:p
    [:em [:strong [:code "This is important"]]]]])

(deftest test-weave|nested-formatting
  (is (= (stitched
          ["_**`This is important`**_"])
         (weave/weave test-env weave-doc|nested-formatting))))

(def weave-doc|div
  [::weave/doc
   [:div
    [:p "To make it easier to break up documents..."]
    [:p "...a division element is helpful."]]])

(deftest test-weave|div
  (is (= (stitched
          ["To make it easier to break up documents..."
           ""
           "...a division element is helpful."])
         (weave/weave test-env weave-doc|div))))
