# Weave: Markdown Output #

This library implements rudimentary Markdown output for [Weave](https://gitlab.com/glossa/weave) documents. This output format is designed for reading directly (e.g., at the REPL), so it does _not_ support arbitrary tags or HTML output.

```clj
;; Git dep
{dev.glossa/weave-markdown
 {:git/url "https://gitlab.com/glossa/weave-markdown.git"
  :git/tag "v0.2.22"
  :git/sha "4e0bbaca87f65be58c902937e45b7714b993cc20"}}

;; Maven dep at https://clojars.org/dev.glossa/weave-markdown
{dev.glossa/weave-markdown {:mvn/version "0.2.22"}}
```

Because this is Weave's default output format, no further configuration is required:

```clj
(require '[glossa.weave :as weave])
(weave/weave [:h1 "Markdown Output!"])
```

## Development ##

_NB: Review the scripts and `deps.edn` files for necessary aliases (not all of which are defined in this repo)._

Please run the setup script to populate Git hooks:

```shell
./script/setup
```

To start nREPL (bring your own `:cider` alias):

```shell
./script/dev
```

To run tests:

```shell
./script/test
```

To run everything that CI does:

```shell
./script/ci
```

## License ##

Copyright © Daniel Gregoire, 2021

THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF THIS ECLIPSE PUBLIC LICENSE ("AGREEMENT"). ANY USE, REPRODUCTION OR DISTRIBUTION OF THE PROGRAM CONSTITUTES RECIPIENT'S ACCEPTANCE OF THIS AGREEMENT.
