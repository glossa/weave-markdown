(ns glossa.weave.output.markdown
  (:require
   [clojure.string :as str]
   [glossa.weave.api :as api :refer [pop1 pop2]]
   [glossa.weave.ir :as ir]))

(def output-format
  {:encoding "UTF-8"
   :format :glossa.weave.output/markdown
   :locale {:language "en"
            :country "US"}})

;;
;; Emit
;;

(defmethod api/emit [:blockquote output-format]
  ;; In that Markdown requires prefixing each line with `>`.
  [env instructions node]
  (let [node (update node
                     :content
                     (fn [enfants]
                       (mapv
                        (fn [enfant]
                          {:tag ::ir/quote-paragraph-child
                           :attrs {}
                           :content [enfant]})
                        enfants)))]
    (api/emit-default env instructions node)))

(def ^:private indent-syntax "   ")
(defn- indent [depth]
  (str/join "" (repeat (inc (or depth 0)) indent-syntax)))

(defn- nestable-list-item-node
  [env {:keys [attrs] :as node}]
  (let [item-depth (::ir/item-depth attrs)
        _ (when-not item-depth
            (throw (ex-info "Developer error. List items must be given a depth by parent elements, e.g., `:ol` or `:ul`."
                            {:error :item-depth-not-set})))
        nested-list-idx (volatile! nil)
        node (update node
                     :content
                     (fn [items]
                       (vec
                        (map-indexed
                         (fn [idx
                              {item-tag :tag
                               :as item-node}]
                           ;; TODO Consider hierarchy and isa? instead of this.
                           (if (or (api/emits-ir? item-tag ::ir/ordered-list (:ir-mapping env))
                                   (api/emits-ir? item-tag ::ir/unordered-list (:ir-mapping env))
                                   (api/emits-ir? item-tag ::ir/description-list (:ir-mapping env)))
                             (do
                               (when-not @nested-list-idx (vreset! nested-list-idx idx))
                               (assoc-in item-node [:attrs ::ir/item-depth] (inc item-depth)))
                             item-node))
                         items))))]
    (assoc-in node [:attrs ::ir/nested-list-idx] @nested-list-idx)))

(defmethod api/emit [::ir/ordered-list-item output-format]
  [env instructions node]
  (let [node (nestable-list-item-node env node)
        {::ir/keys [item-depth item-index nested-list-idx]} (:attrs node)
        prefix [(indent item-depth) (str (inc item-index)) ". "]
        node (update node
                     :content
                     (fn [content]
                       (let [content (if nested-list-idx
                                       (vec
                                        (concat
                                         (take nested-list-idx content)
                                         ["\n"]
                                         (drop nested-list-idx content)))
                                       (conj content "\n"))]
                         (into prefix content))))]
    (api/emit-default env instructions node)))

(defmethod api/emit [::ir/unordered-list-item output-format]
  [env instructions node]
  (let [node (nestable-list-item-node env node)
        {::ir/keys [item-depth nested-list-idx]} (:attrs node)
        prefix [(indent item-depth) "- "]
        node (update node
                     :content
                     (fn [content]
                       (let [content (if nested-list-idx
                                       (concat
                                        (take nested-list-idx content)
                                        ["\n"]
                                        (drop nested-list-idx content))
                                       (conj content "\n"))]
                         (into prefix content))))]
    (api/emit-default env instructions node)))

(defmethod api/emit [:dl output-format]
  [env instructions {:keys [attrs] :as node}]
  (let [item-depth (::ir/item-depth attrs 0)
        node (update node
                     :content
                     (fn [items]
                       (mapv
                        (fn [[dt dd]]
                          {:tag ::ir/unordered-list-item
                           :attrs {::ir/original-tag (:tag dt)
                                   ::ir/item-depth item-depth}
                           :content [dt dd]})
                        (partition-all 2 items))))
        node (if (zero? item-depth)
               (update node :content conj "\n")
               node)]
    (api/emit-default env instructions node)))

(defmethod api/emit [:ol output-format]
  ;; Weave syntax is to use generic :li or :item.
  [env instructions {:keys [attrs] :as node}]
  (let [counter (volatile! -1)
        item-depth (::ir/item-depth attrs 0)
        node (update node
                     :content
                     (fn [items]
                       (mapv
                        (fn [{item-tag :tag
                              :as item}]
                          (let [item (if (api/emits-ir? item-tag ::ir/list-item (:ir-mapping env))
                                       (-> item
                                           (assoc :tag ::ir/ordered-list-item)
                                           (assoc-in [:attrs ::ir/original-tag] item-tag)
                                           (assoc-in [:attrs ::ir/item-index] (vswap! counter inc)))
                                       item)]
                            (assoc-in item [:attrs ::ir/item-depth] item-depth)))
                        items)))
        node (if (zero? item-depth)
               (update node :content conj "\n")
               node)]
    (api/emit-default env instructions node)))

(defmethod api/emit [:ul output-format]
  ;; Weave syntax is to use generic :li or :item.
  [env instructions {:keys [attrs] :as node}]
  (let [item-depth (::ir/item-depth attrs 0)
        node (update node
                     :content
                     (fn [items]
                       (mapv
                        (fn [{item-tag :tag :as item}]
                          (let [item (if (api/emits-ir? item-tag ::ir/list-item (:ir-mapping env))
                                       (-> item
                                           (assoc :tag ::ir/unordered-list-item)
                                           (assoc-in [:attrs ::ir/original-tag] item-tag))
                                       item)]
                            (assoc-in item [:attrs ::ir/item-depth] item-depth)))
                        items)))
        node (if (zero? item-depth)
               (update node :content conj "\n")
               node)]
    (api/emit-default env instructions node)))

;;
;; Render
;;

(defmethod api/render [::ir/bold output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack (str "**" text "**"))))

(defmethod api/render [::ir/description-definition output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack text)))

(defmethod api/render [::ir/description-list output-format]
  [env stack _]
  (let [[stack _] (pop1 stack)]
    {:env env
     :stack stack}))

(defmethod api/render [::ir/description-term output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack (str "**" text "** "))))

(defmethod api/render [::ir/division output-format]
  [env stack _]
  (let [[stack _] (pop1 stack)]
    {:env env
     :stack stack}))

(defmethod api/render [::ir/doc output-format]
  [env stack _]
  (let [[stack _] (pop1 stack)]
    {:env env
     :stack stack}))

(defmethod api/render [::ir/emphasis output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack (str "_" text "_"))))

(defmethod api/render [::ir/heading output-format]
  [env stack _]
  (let [[stack [attrs text]] (pop2 stack)
        {:keys [level]} attrs
        heading-num (inc level)
        syntax (str/join "" (repeat heading-num "#"))]
    (api/render-default env stack (str syntax " " text " " syntax "\n\n"))))

(defmethod api/render [::ir/horizontal-line output-format]
  [env stack _]
  (let [[stack [_ _]] (pop2 stack)]
    (api/render-default env stack "--------------------------------------------------------------------------------\n\n")))

(defmethod api/render [::ir/italic output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack (str "_" text "_"))))

(defmethod api/render [::ir/line-break output-format]
  [env stack _]
  (let [[stack [_ _]] (pop2 stack)]
    (api/render-default env stack "\n")))

(defmethod api/render [::ir/link output-format]
  [env stack _]
  (let [[stack [attrs text]] (pop2 stack)]
    (api/render-default env stack (str "[" text "](" (:url attrs) ")"))))

(defmethod api/render [::ir/literal output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack (str "`" text "`"))))

(defn- show-whitespace
  [s]
  (-> s
      (str/replace " " "␣")
      (str/replace "\t" "»   ")
      (str/replace "\r\n" "↵\n")
      (str/replace "\n" "↓\n")))

(defmethod api/render [::ir/literal-show-whitespace output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)
        text (show-whitespace text)]
    (api/render-default env stack (str "`" text "`"))))

(defmethod api/render [::ir/literal-paragraph-show-whitespace output-format]
  [env stack _]
  (let [[stack [attrs text]] (pop2 stack)
        code-fence-start (str "```"
                              (when-let [lang (:lang attrs)]
                                (name lang))
                              "\n")
        code-fence-end "\n```\n\n"
        text (show-whitespace text)]
    (api/render-default env stack (str code-fence-start text code-fence-end))))

(defmethod api/render [::ir/literal-paragraph output-format]
  [env stack _]
  (let [[stack [attrs text]] (pop2 stack)
        code-fence-start (str "```"
                              (when-let [lang (:lang attrs)]
                                (name lang))
                              "\n")
        code-fence-end "\n```\n\n"]
    (api/render-default env stack (str code-fence-start text code-fence-end))))

(defmethod api/render [::ir/ordered-list output-format]
  [env stack _]
  (let [[stack _attrs] (pop1 stack)]
    {:env env
     :stack stack}))

(defmethod api/render [::ir/ordered-list-item output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack text)))

(defmethod api/render [::ir/paragraph output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack (str text "\n\n"))))

(defmethod api/render [::ir/quote-paragraph output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack (str text "\n\n"))))

(defmethod api/render [::ir/quote-paragraph-child output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack (str "> " text "\n"))))

(defmethod api/render [::ir/strike-through output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack (str "~" text "~"))))

(defmethod api/render [::ir/string output-format]
  [env stack _]
  (let [[stack [_ string]] (pop2 stack)]
    (api/render-default env stack string)))

(defmethod api/render [::ir/unordered-list-item output-format]
  [env stack _]
  (let [[stack [_ text]] (pop2 stack)]
    (api/render-default env stack text)))

(defmethod api/render [::ir/unordered-list output-format]
  [env stack _]
  (let [[stack _attrs] (pop1 stack)]
    {:env env
     :stack stack}))
